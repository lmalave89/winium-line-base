package calculator;

import org.junit.Test;

import functions.Generics;

import org.junit.After;
import org.junit.Before;
import pageObjects.LoginPage;

import java.io.IOException;
import injectionDependencies.TextContext;



public class Calculator{
	
	TextContext textContext;
	LoginPage loginPage;	
	
	@Before
	public void setup() throws IOException, InterruptedException   {
		textContext = new TextContext();
		loginPage = textContext.getPageObjectManager().getLoginPage();
	}

	@Test
	public void add() throws InterruptedException {
		Generics.Click(loginPage.number_9);
		Generics.Click(loginPage.add);
		Generics.Click(loginPage.number_5);
		Generics.Click(loginPage.equal);
		Generics.ValidateTextUsingName(loginPage.result, "14");
	}
	
	@Test
	public void subtract() throws InterruptedException {
		Generics.Click(loginPage.number_9);
		Generics.Click(loginPage.subtract);
		Generics.Click(loginPage.number_5);
		Generics.Click(loginPage.equal);
		Generics.ValidateTextUsingName(loginPage.result, "4");
	}
	
	@After
	public void end() throws InterruptedException {
		textContext.getDriverManager().closeDriver();
	}	
}
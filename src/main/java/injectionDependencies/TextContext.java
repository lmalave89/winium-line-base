package injectionDependencies;

import java.io.IOException;
import managers.DriverManager;
import managers.PageObjectManager;

public class TextContext {

	private DriverManager driverManager;
	private PageObjectManager pageObjectManager;

	
	public TextContext() throws InterruptedException, IOException {
		driverManager = new DriverManager();
		pageObjectManager = new PageObjectManager(driverManager.getDriver());

	}
	
	public DriverManager getDriverManager() {
		return driverManager;
	}
	
	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}


}

package managers;

import org.openqa.selenium.winium.WiniumDriver;

import pageObjects.LoginPage;

public class PageObjectManager {

	private WiniumDriver driver;
	private LoginPage loginPage;
	
	
	public PageObjectManager(WiniumDriver driver) {
		this.driver = driver;			
	}
	
	public LoginPage getLoginPage(){		
		return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
	}	

}

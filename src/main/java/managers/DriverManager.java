package managers;

import java.io.IOException;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

public class DriverManager {

	static WiniumDriver driver;
	
	public WiniumDriver getDriver() throws IOException {
		DesktopOptions options = new DesktopOptions();
		options.setApplicationPath(FileReaderManager.getInstance().getConfigReader().getApplication());
		driver = new WiniumDriver(new URL(FileReaderManager.getInstance().getConfigReader().getHost()), options);
		return driver;
	}

	public void closeDriver() throws InterruptedException {
		Thread.sleep(FileReaderManager.getInstance().getConfigReader().getTime());
		driver.findElement(By.name("Close")).click();
	}

}

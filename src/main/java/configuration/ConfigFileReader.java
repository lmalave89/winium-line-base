package configuration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {

	
	private Properties properties;
	private final String propertyFilePath= "config/configurations.properties";

	public ConfigFileReader() {

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}

	
	public int getTime() {		
		String time = properties.getProperty("time");
		if(time != null) return Integer.valueOf(time);
		else throw new RuntimeException("time not specified in the Configuration.properties file.");		
	}
	
	public String getApplication() {
		String application = properties.getProperty("application");
		if(application != null) return application;
		else throw new RuntimeException("application not specified in the Configuration.properties file.");
	}
	
	public String getHost() {
		String host = properties.getProperty("host");
		if(host != null) return host;
		else throw new RuntimeException("host not specified in the Configuration.properties file.");
	}
 

}

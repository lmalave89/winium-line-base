package functions;

import static org.junit.Assert.*;

import org.openqa.selenium.WebElement;

import managers.FileReaderManager;

public class Generics {
	
	static int time = FileReaderManager.getInstance().getConfigReader().getTime();
	
	public static void Click(WebElement element) throws InterruptedException {
		Thread.sleep(time);
		element.click();
	}
	
	public static void ValidateTextUsingName(WebElement element, String value) throws InterruptedException {
		Thread.sleep(time);
		assertEquals(element.getAttribute("Name"), value);
	}

}

package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.winium.WiniumDriver;

public class LoginPage {

	WiniumDriver driver;
	
	public LoginPage(WiniumDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.NAME, using = "9") 
	public  WebElement number_9;
	
	@FindBy(how = How.NAME, using = "5") 
	public  WebElement number_5;
	
	@FindBy(how = How.NAME, using = "Add") 
	public  WebElement add;
	
	@FindBy(how = How.NAME, using = "Subtract") 
	public  WebElement subtract;
	
	
	
	@FindBy(how = How.NAME, using = "Equals") 
	public  WebElement equal;
	
	@FindBy(how = How.ID, using = "150") 
	public  WebElement result;
	
	@FindBy(how = How.NAME, using = "Close") 
	public WebElement close;


}
